<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class TarifaController extends AbstractController
{
    /**
     * @Route("/tarifa/nuevo", name="tarifa")
     */
    public function nuevo(Request $request)
    {
        $tarifa = new \App\Entity\Tarifa();
        $form = $this->createForm(\App\Form\TarifaType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
           $tarifa=$this->getDoctrine()->getRepository(\App\Entity\Tarifa::class)->nuevo($request->get('tarifa')['nombre']);
            if ($tarifa==false){
                $this->addFlash('alert alert-success','Tarifa ya Existe');
            }
            
            
            
                    
        }
        
        return $this->render('tarifa/nuevo.html.twig', [
          'form' => $form->createView()
        ]);
    }
}
