<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoriaController extends AbstractController {

    /**
     * @Route("/categoria", name="categoria")
     */
    public function index() {
        return $this->render('categoria/index.html.twig', [
                    'controller_name' => 'CategoriaController',
        ]);
    }

    /**
     * @Route("/categoria/nuevo", name="nuevo")
     */
    public function nuevo(Request $request) {
        $categoria = new \App\Entity\Categoria();
        $form = $this->createForm(\App\Form\CategoriaType::class, $categoria);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$categoria->setNombre($this->)
            $em->persist($categoria);
            try {
                $em->flush();
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        return $this->render('categoria/nuevo.html.twig', [
                    'form' => $form->createView()
        ]);
    }

}
