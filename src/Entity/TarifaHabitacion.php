<?php

namespace App\Entity;

use App\Repository\TarifaHabitacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifaHabitacionRepository::class)
 */
class TarifaHabitacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity=Tarifa::class, inversedBy="tarifaHabitacions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tarifa;

    /**
     * @ORM\ManyToOne(targetEntity=Habitacion::class, inversedBy="tarifaHabitacions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $habitacion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrecio(): ?int
    {
        return $this->precio;
    }

    public function setPrecio(int $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    
    public function getTarifa(): ?Tarifa
    {
        return $this->tarifa;
    }

    public function setTarifa(?Tarifa $tarifa): self
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    public function getHabitacion(): ?Habitacion
    {
        return $this->habitacion;
    }

    public function setHabitacion(?Habitacion $habitacion): self
    {
        $this->habitacion = $habitacion;

        return $this;
    }
}
