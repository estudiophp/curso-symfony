<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $administrador;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechacreacion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechamodificacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $creado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modificado;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $telefono;

    public function getId(): ?int {
        return $this->id;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return (string) $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt() {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self {
        $this->apellido = $apellido;

        return $this;
    }

    public function getImg(): ?string {
        return $this->img;
    }

    public function setImg(?string $img): self {
        $this->img = $img;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    public function getAdministrador(): ?bool {
        return $this->administrador;
    }

    public function setAdministrador(bool $administrador): self {
        $this->administrador = $administrador;

        return $this;
    }

    public function getFechacreacion(): ?\DateTimeInterface {
        return $this->fechacreacion;
    }

    public function setFechacreacion(\DateTimeInterface $fechacreacion): self {
        $this->fechacreacion = $fechacreacion;

        return $this;
    }

    /**
     * @ORM\PreUpdate 
     */
    public function getFechamodificacion(): ?\DateTimeInterface {
        return $this->fechamodificacion;
    }

    public function setFechamodificacion(\DateTimeInterface $fechamodificacion): self {
        $this->fechamodificacion = $fechamodificacion;

        return $this;
    }

    public function getCreado(): ?string {
        return $this->creado;
    }

    public function setCreado(string $creado): self {
        $this->creado = $creado;

        return $this;
    }

    public function getModificado(): ?string {
        return $this->modificado;
    }

    public function setModificado(string $modificado): self {
        $this->modificado = $modificado;

        return $this;
    }

    public function getTelefono(): ?int {
        return $this->telefono;
    }

    public function setTelefono(int $telefono): self {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreaciondevalor() {
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
    }

}
