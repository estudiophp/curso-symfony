<?php

namespace App\Entity;

use App\Repository\CategoriaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriaRepository::class)
 */
class Categoria
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img;

    /**
     * @ORM\OneToMany(targetEntity=Habitacion::class, mappedBy="categoria")
     */
    private $habitacions;

    public function __construct()
    {
        $this->habitacions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection|Habitacion[]
     */
    public function getHabitacions(): Collection
    {
        return $this->habitacions;
    }

    public function addHabitacion(Habitacion $habitacion): self
    {
        if (!$this->habitacions->contains($habitacion)) {
            $this->habitacions[] = $habitacion;
            $habitacion->setCategoria($this);
        }

        return $this;
    }

    public function removeHabitacion(Habitacion $habitacion): self
    {
        if ($this->habitacions->contains($habitacion)) {
            $this->habitacions->removeElement($habitacion);
            // set the owning side to null (unless already changed)
            if ($habitacion->getCategoria() === $this) {
                $habitacion->setCategoria(null);
            }
        }

        return $this;
    }
}
