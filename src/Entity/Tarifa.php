<?php

namespace App\Entity;

use App\Repository\TarifaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TarifaRepository::class)
 */
class Tarifa
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity=TarifaHabitacion::class, mappedBy="tarifa")
     */
    private $tarifaHabitacions;

    public function __construct()
    {
        $this->tarifaHabitacions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|TarifaHabitacion[]
     */
    public function getTarifaHabitacions(): Collection
    {
        return $this->tarifaHabitacions;
    }

    public function addTarifaHabitacion(TarifaHabitacion $tarifaHabitacion): self
    {
        if (!$this->tarifaHabitacions->contains($tarifaHabitacion)) {
            $this->tarifaHabitacions[] = $tarifaHabitacion;
            $tarifaHabitacion->setTarifa($this);
        }

        return $this;
    }

    public function removeTarifaHabitacion(TarifaHabitacion $tarifaHabitacion): self
    {
        if ($this->tarifaHabitacions->contains($tarifaHabitacion)) {
            $this->tarifaHabitacions->removeElement($tarifaHabitacion);
            // set the owning side to null (unless already changed)
            if ($tarifaHabitacion->getTarifa() === $this) {
                $tarifaHabitacion->setTarifa(null);
            }
        }

        return $this;
    }
}
