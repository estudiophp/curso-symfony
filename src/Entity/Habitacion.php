<?php

namespace App\Entity;

use App\Repository\HabitacionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HabitacionRepository::class)
 */
class Habitacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity=Categoria::class, inversedBy="habitacions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity=TarifaHabitacion::class, mappedBy="habitacion")
     */
    private $tarifaHabitacions;

    public function __construct()
    {
        $this->tarifaHabitacions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|TarifaHabitacion[]
     */
    public function getTarifaHabitacions(): Collection
    {
        return $this->tarifaHabitacions;
    }

    public function addTarifaHabitacion(TarifaHabitacion $tarifaHabitacion): self
    {
        if (!$this->tarifaHabitacions->contains($tarifaHabitacion)) {
            $this->tarifaHabitacions[] = $tarifaHabitacion;
            $tarifaHabitacion->setHabitacion($this);
        }

        return $this;
    }

    public function removeTarifaHabitacion(TarifaHabitacion $tarifaHabitacion): self
    {
        if ($this->tarifaHabitacions->contains($tarifaHabitacion)) {
            $this->tarifaHabitacions->removeElement($tarifaHabitacion);
            // set the owning side to null (unless already changed)
            if ($tarifaHabitacion->getHabitacion() === $this) {
                $tarifaHabitacion->setHabitacion(null);
            }
        }

        return $this;
    }
}
