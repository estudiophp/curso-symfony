<?php

namespace App\Entity;

use App\Repository\PersonaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonaRepository::class)
 */
class Persona
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipodocumento;

    /**
     * @ORM\Column(type="string", length=24)
     */
    private $numdocumento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;
    /**
     * @ORM\Column(type="string", length=24)
     */
    private $sexo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechanacimiento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipodocumento(): ?string
    {
        return $this->tipodocumento;
    }

    public function setTipodocumento(string $tipodocumento): self
    {
        $this->tipodocumento = $tipodocumento;

        return $this;
    }

    public function getNumdocumento(): ?string
    {
        return $this->numdocumento;
    }

    public function setNumdocumento(string $numdocumento): self
    {
        $this->numdocumento = $numdocumento;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }
    

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }
    
    public function getSexo(): ?string
    {
        return $this->sexo;
    }
    
    
    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }
    
    
    

    public function getFechanacimiento(): ?\DateTimeInterface
    {
        return $this->fechanacimiento;
    }

    public function setFechanacimiento(?\DateTimeInterface $fechanacimiento): self
    {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }
}
