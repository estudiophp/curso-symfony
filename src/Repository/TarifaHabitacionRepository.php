<?php

namespace App\Repository;

use App\Entity\TarifaHabitacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TarifaHabitacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method TarifaHabitacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method TarifaHabitacion[]    findAll()
 * @method TarifaHabitacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaHabitacionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TarifaHabitacion::class);
    }

    // /**
    //  * @return TarifaHabitacion[] Returns an array of TarifaHabitacion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TarifaHabitacion
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
