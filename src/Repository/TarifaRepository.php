<?php

namespace App\Repository;

use App\Entity\Tarifa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Twig\Environment;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * @method Tarifa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarifa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarifa[]    findAll()
 * @method Tarifa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarifaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, Environment $twig)
    {
        parent::__construct($registry, Tarifa::class);
    }
    public function nuevo($nombre){
        $tarifa =new Tarifa();
        $tarifa->setNombre($nombre);
        $manager = $this->getEntityManager();
        $manager->persist($tarifa);
        try {
            if($manager->flush()== false ){
                return true;
            }
            else {
                return false;
            }
        }catch (UniqueConstraintViolationException $exception) {
            
            return false;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
            
    }

    // /**
    //  * @return Tarifa[] Returns an array of Tarifa objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tarifa
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
