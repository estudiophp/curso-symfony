<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{
    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
            $usuario = new \App\Entity\User();
            $usuario->setEmail("intillaysuyayniy1@gmail.com");
            $usuario->setRoles(['ROLE_USER']);
            $usuario->setPassword($this->passwordEncoder->encodePassword($usuario,'intillay'));
            $usuario->setNombre("Intillay");
            $usuario->setApellido("Soto Lacma");
            //$usuario->setImg();
            $usuario->setActivo(true);
            $usuario->setAdministrador(true);
            
            
            $usuario->setCreado("Administrador");
            $usuario->setModificado("Administrador");
            $usuario->setTelefono(928877463);
            $manager->persist($usuario);
        $manager->flush();
    }
}
