<?php

namespace App\Form;

use App\Entity\Tarifa;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class TarifaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class,[
                'label' =>'Tarifa',
                'attr' => [
                    'placeholder' => 'Nombre de la Tarifa',
                    'maxlength' => 50,
                ],
            ])
            ->add('save',SubmitType::class,[
                'label' =>'Guardar',
                'attr' => [
                    'class' => 'btn btn-flight',
                    
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tarifa::class,
        ]);
    }
}
