<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201020015957 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD nombre VARCHAR(255) NOT NULL, ADD apellido VARCHAR(255) NOT NULL, ADD img VARCHAR(255) DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD administrador TINYINT(1) NOT NULL, ADD fechacreacion DATETIME NOT NULL, ADD fechamodificacion DATETIME NOT NULL, ADD creado VARCHAR(255) NOT NULL, ADD modificado VARCHAR(255) NOT NULL, ADD telefono INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP nombre, DROP apellido, DROP img, DROP activo, DROP administrador, DROP fechacreacion, DROP fechamodificacion, DROP creado, DROP modificado, DROP telefono');
    }
}
